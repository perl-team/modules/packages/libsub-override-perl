libsub-override-perl (0.12-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.12.
  * Update (test) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Jun 2024 02:50:03 +0200

libsub-override-perl (0.11-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.11.
  * New test and runtime dependency: libsub-prototype-perl.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 May 2024 16:58:09 +0200

libsub-override-perl (0.10-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.10.
  * Update debian/upstream/metadata.
  * Update years of upstream copyright.
  * Remove unused test dependencies.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Dec 2023 18:54:36 +0100

libsub-override-perl (0.09-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libsub-override-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 16:00:22 +0100

libsub-override-perl (0.09-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:05:32 +0100

libsub-override-perl (0.09-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3

 -- Niko Tyni <ntyni@debian.org>  Thu, 18 Jan 2018 17:06:30 +0200

libsub-override-perl (0.09-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.09
  * Switch build-dependency on Test::Exception to Test::Fatal
  * Bump dh compatibility to level 8 (no changes necessary)
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0, directly
    link GPL-1, do not reference "GNU/Linux" in License paragraphs)
  * Drop spelling.patch, applied upstream
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 20 Sep 2013 12:36:39 +0200

libsub-override-perl (0.08-4) unstable; urgency=low

  * Now maintained by the Debian Perl Group. (Closes: #576181)
    + debian/control: Add Vcs-* fields.
    + debian/control: Add myself to Uploaders.
  * debian/control: Add Homepage field.
  * debian/control: Move debhelper to Build-Depends, drop build-dep on
    libtest-simple-perl, add build-dep on libtest-pod-perl and
    libtest-pod-coverage-perl to enable additional tests.
  * debian/control: Add ${misc:Depends}.
  * Refresh rules for debhelper 7.
  * Use source format 3.0 (quilt).
  * Fix spelling error in documentation.
    + new patch: spelling.patch
  * Convert debian/copyright to proposed machine-readable format.
  * debian/watch: Use dist-based URL.
  * Bump Standards-Version to 3.8.4.

 -- Ansgar Burchardt <ansgar@43-1.org>  Thu, 01 Apr 2010 20:50:30 +0900

libsub-override-perl (0.08-3) unstable; urgency=low

  * New maintainer (closes: #331099).

 -- Víctor Pérez Pereira <vperez@debianvenezuela.org>  Sat, 25 Feb 2006 19:04:34 -0400

libsub-override-perl (0.08-2) unstable; urgency=low

  * Orphaning package.
  * Changed maintainer to "Debian QA Group" per standard procedure

 -- Kenneth J. Pronovici <pronovic@debian.org>  Fri,  7 Oct 2005 11:57:53 -0500

libsub-override-perl (0.08-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.6.2; no packaging changes.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat,  1 Oct 2005 10:32:25 -0500

libsub-override-perl (0.06-1) unstable; urgency=low

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon,  6 Dec 2004 17:02:22 -0600

libsub-override-perl (0.05-1) unstable; urgency=low

  * Initial release (closes: #276606).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Thu, 14 Oct 2004 20:15:09 -0500
